# Lancement des script
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

require 'rubygems'
require './src/batch/CommandeRecupBatch'

def recupcommande
  puts "======================================="
  puts "Lancement du batch 'CommandeRecupBatch'"
  puts "=======================================\n\n"
  batch = CommandeRecupBatch.new()
  batch.insertCommande()
  puts "\n======================================="
  puts "  Fin du batch 'CommandeRecupBatch'"
  puts "======================================="
end

vArgumentsPossibles = "Arguments possibles :\n* recupcommande\n* help"

if ARGV[0]
  case ARGV[0] 
  when "help"
    puts "Bienvenu dans l'aide.\nVous devez saisir un argument après la commande.\n#{vArgumentsPossibles}" 
  when "recupcommande"
    recupcommande
  else
    puts "Argument '#{ARGV[0]}' inconnu.\n#{vArgumentsPossibles}"
  end
else
  puts "Aucun argument saisi.\nLancement du batch recupcommande par défaut.\n\n"
  recupcommande
end


# Implémentation de la classe CsvDb
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

class CsvDb
  
  attr_accessor :csv, :data, :header
    
  def initialize(csv=nil, header_first_line=true)
    @csv = csv
    @data = []
    @header_first_line = header_first_line
    @header = []
    @orders = []
  end
  
  def load
    @fields = []
    @data = []
    i=0
    csv = CSV.open(@csv,"rb:ISO-8859-1:UTF-8",{:col_sep => ";"})
    csv.each {|line|
      if (i==0 and @header_first_line)
        @header = line
      else
        @data << line
      end
      i += 1
    }
  end

end
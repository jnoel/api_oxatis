# Implémentation de la classe AccessDb
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

class AccessDb
  attr_accessor :mdb, :connection, :data, :fields, :catalog
   
  def initialize(mdb=nil)
    @mdb = mdb
    @connection = nil
    @data = nil
    @fields = nil
  end
 
  def open
    connection_string = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='
    #Access 2010 connection string
    # connection_string = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='
    connection_string << @mdb
    @connection = WIN32OLE.new('ADODB.Connection')
    @connection.Open(connection_string)
     
    @catalog = WIN32OLE.new("ADOX.Catalog")
    @catalog.ActiveConnection = @connection
     
    # catalog.create(connection_string)
    # Source="#{@mdb_file}"
  end
 
  def query(sql)
    recordset = WIN32OLE.new('ADODB.Recordset')
    recordset.Open(sql, @connection)
    @fields = []
    recordset.Fields.each do |field|
      @fields << field.Name
    end
    begin
      @data = recordset.GetRows.transpose
    rescue
      @data = []
    end
    recordset.Close
  end
 
  def tables
    tables = []
    @catalog.tables.each {|t| tables << t.name if t.type == "TABLE" }
    tables
  end
 
  def execute(sql)
    @connection.Execute(sql)
  end
  
  def insert(pTable, pEntetes, pValues)
    req = "INSERT INTO #{pTable} (#{pEntetes}) VALUES ("
    pValues.each do |vValue|
      if vValue.is_a? String
        req << "'#{vValue.gsub("'","''")}',"
      else
        req << "'#{vValue}',"
      end
    end
    req.chop!
    req << ")"
    #p req
    execute(req)
  end
 
  def close
    @connection.Close
  end
end
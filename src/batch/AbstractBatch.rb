# Implémentation de la classe AbstractBatch
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

require 'win32ole'
require 'csv'
require 'yaml'
require './src/model/Client'
require './src/model/Commande'
require './src/model/CommandeLigne'
require './src/dao/db/ClientDaoDb'
require './src/dao/db/CommandeDaoDb'
require './src/dao/db/CommandeLigneDaoDb'
require './src/dao/csv/CommandeDaoCsv'
require './src/helper/AccessDb'
require './src/helper/CsvDb'

class AbstractBatch
  attr_accessor :vApiFile, :vOxatisFile, :vApiConnexion
  
  def initialize
    conf = YAML.load_file('./configuration.yaml')
    @vApiFile=conf["ApiFile"]
    @vOxatisFile=conf["OxatisFile"]
    
    @vApiConnexion = AccessDb.new(@vApiFile) if File.exist?(@vApiFile)
    
    @vOxatisConnexion = CsvDb.new(@vOxatisFile) if File.exist?(@vOxatisFile)
  end
  
end
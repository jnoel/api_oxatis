# Implémentation de la classe CommandeRecupBatch
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

require './src/batch/AbstractBatch'

class CommandeRecupBatch < AbstractBatch
  
  def initialize
    super()
    @vListCommande = nil
  end
  
  def recupCommande
    if @vOxatisConnexion
      @vOxatisConnexion.load
      @vCommandeDaoCsv = CommandeDaoCsv.new(@vOxatisConnexion)
      return @vCommandeDaoCsv.getListCommande
    else
      return []
    end
  end
  
  def insertCommande  
    @vListCommande = recupCommande unless @vListCommande
    
    vRetour = []
    unless @vListCommande.empty?
      if @vApiConnexion
        @vApiConnexion.open
        @vListCommande.each do |vCmde|   
            @vCommandeDaoDb = CommandeDaoDb.new(@vApiConnexion)
            vRetour << @vCommandeDaoDb.insertCommande(vCmde)    
        end
        @vApiConnexion.close
      else
        vRetour << [false, [{:type => "KO", :message => "Aucun accès à la base de données API !\nVeuillez vérifier la configuration."}]]
      end
    else
      vRetour << [false, [{:type => "KO", :message => "Aucune commande à importer !"}]]
    end
    
    return vRetour
  end
  
end

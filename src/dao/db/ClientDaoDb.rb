# Implémentation de la classe ClientDaoDb
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

class ClientDaoDb
  
  def initialize(pConnexion)
    @connexion = pConnexion
    @entete = "Code, FacNom, FacCivilite, FacAdr, FacCp, FacVille, DateCreation, FacEMail, FacTel, "\
             +"LivNom, LivCivilite, LivAdr, LivCp, LivVille, LivTel"
  end
  
  def getClient(pClient)
    
    req = "SELECT #{@entete} 
           FROM Client 
           WHERE FacEMail = '#{pClient.email}'"
    
    @connexion.query(req)
    if @connexion.data.count>0
      vData = @connexion.data[0]
      pClient.code = vData[0]
      begin
        pClient.dateCreation = DateTime.parse(vData[6])
      rescue
        
      end
    end
    
    return @connexion.data.count>0
    
  end
  
  def insertClient(pClient)

    vValues = [pClient.code, pClient.facNom, pClient.facCivilite, pClient.facAdr, pClient.facCp, 
              pClient.facVille, pClient.dateCreation.strftime('%d/%m/%y'), pClient.email, pClient.facTel,
              pClient.livNom, pClient.livCivilite, pClient.livAdr, pClient.livCp, pClient.livVille, pClient.livTel]
    vMessage = ""
    vReussite = false
    begin
      @connexion.insert("Client", @entete, vValues)
      vMessage = "Le client dont l'adresse email est #{pClient.email} a été intégré à API"
      vReussite = true
    rescue
      vMessage = "!!!!!!!! Le client dont l'adresse email est #{pClient.email} n'a pas pu être intégré à API\n#{pClient}\n"
    end
    puts vMessage
    
    return [vReussite, vMessage]
  end 
  
end
# Implémentation de la classe CommandeDaoDb
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

class CommandeDaoDb
  
  def initialize(pConnexion)
    @connexion = pConnexion
    @entete = "Code, Type, [Date], CodeTiers, Nom, Civilite, Adr, Cp, Ville, Tarif, TypeDeTarif, "\
              + " Devise, CategorieAV, TotalTTC, MonnaieRef_TotalTTC, MonnaieRef_NetAPayer, TotalTvaNet1, "\
              + "Civilite_Liv, Nom_Liv, Adr_Liv, Cp_Liv, Ville_Liv "
  end
  
  def getCommande(pCode)
    
    vCommande = nil
    
    req = "SELECT #{@entete} 
           FROM EnteteCdeClient 
           WHERE Code = '#{pCode}'"
    
    @connexion.query(req)
    if @connexion.data.count>0
      vData = @connexion.data[0]
      vCommande = Commande.new(pCode)
      vCommande.type = vData[1]
      vCommande.date = vData[21]
      vCommande.client.code = vData[3]
      vCommande.client.facNom = vData[4]
      vCommande.client.facCivilite = vData[5]
      vCommande.client.facAdr = vData[6]
      vCommande.client.facCp = vData[7]
      vCommande.client.facVille = vData[8]
      vCommande.tarif = vData[9]
      vCommande.typeDeTarif = vData[10]
      vCommande.devise = vData[11]
      vCommande.categorieAV = vData[12]
      vCommande.totalTTC = vData[13]
      vCommande.totalTvaNet1 = vData[16]
    end
    
    return vCommande
    
  end
  
  def insertCommande(pCommande)
    
    vReussite = false
    vListMessage = []
    
    unless getCommande(pCommande.code)
      vClientDaoDb = ClientDaoDb.new(@connexion)
      unless vClientDaoDb.getClient(pCommande.client)
        vRetour = vClientDaoDb.insertClient(pCommande.client)
        vReussite = vRetour[0]
        vListMessage << {:type => (vReussite ? "OK" : "KO"), :message => vRetour[1]}
      end
      
      if vClientDaoDb.getClient(pCommande.client)
        req = "INSERT INTO EnteteCdeClient (#{@entete})"
        vValues=[pCommande.code, pCommande.type, pCommande.date.strftime("%d/%m/%Y"), pCommande.client.code, pCommande.client.facNom, 
                 pCommande.client.facCivilite, pCommande.client.facAdr, pCommande.client.facCp, pCommande.client.facVille, pCommande.tarif, 
                 pCommande.typeDeTarif, pCommande.devise, pCommande.categorieAV, pCommande.totalTTC, pCommande.totalTTC, 
                 pCommande.totalTTC, pCommande.totalTvaNet1, pCommande.client.livCivilite, pCommande.client.livNom, pCommande.client.livAdr,
                 pCommande.client.livCp, pCommande.client.livVille]
        
        begin
          @connexion.insert("EnteteCdeClient", @entete, vValues)
          vMessage = "La commande dont le code est #{pCommande.code} a été intégrée à API"
          puts vMessage
          vListMessage << {:type => "OK", :message => vMessage}
          vReussite = true
        rescue
          vMessage =  "!!!!!!!! La commande dont le code est #{pCommande.code} n'a pas pu être intégrée à API\n#{pCommande}\n"
          puts vMessage
          vListMessage << {:type => "KO", :message => vMessage}
        end
        
        if vReussite
          vCommandeLigneDao = CommandeLigneDaoDb.new(@connexion)
          pCommande.ligneCommande.each do |ligne|
            vRetour = vCommandeLigneDao.insertCommandeLigne(ligne)
            vReussite &= vRetour[0]
            vListMessage << {:type => (vReussite ? "OK" : "KO"), :message => vRetour[1]}
          end
        end
        
      else
        vMessage = "Le client dont l'adresse mail est #{pCommande.client.email} n'existe pas dans la base, annulation de la création de sa commande."
        puts vMessage
        vListMessage << {:type => "KO", :message => vMessage}
      end
    else
      vMessage = "La commande dont le code est #{pCommande.code} existe déjà dans API. Intégration annulée."
      puts vMessage
      vListMessage << {:type => "KO", :message => vMessage}
    end

    #deleteCommande(pCommande.code)
    
    return [vReussite, vListMessage]
    
  end
  
  def deleteCommande(pCommandeId)
    vCommandeLigneDao = CommandeLigneDaoDb.new(@connexion)
    vCommandeLigneDao.deleteCommandeLigne(pCommandeId)
    
    req = "DELETE FROM EnteteCdeClient WHERE Code='#{pCommandeId}'"
    @connexion.execute(req)
    puts "Suppression de la commande dont le code est #{pCommandeId}"
  end
  
end
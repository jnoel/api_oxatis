# Implémentation de la classe CommandeLigneDaoDb
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

class CommandeLigneDaoDb
  
  def initialize(pConnexion)
    @connexion = pConnexion
    @entete = "CodeDocument, NumeroLigne, CodeArticle, TypeLigne, Designation,"\
              +"Quantite, PrixUnitaire, PrixTotalLigne, TotalLigneHT, TotalTTCLigne,"\
              +"CodeTVA, TauxTVA, CodeTiers"
  end
  
  def getListCommandeLigne(pCode)
    
    vListCommandeLigne = []
    
    req = "SELECT #{@entete} 
           FROM LigneCdeClient 
           WHERE Code = '#{pCode}'"
    
    @connexion.query(req)
    @connexion.data.each do |vData|
      vCommandeLigne = CommandeLigne.new()
      vCommandeLigne.codeDocument = vData[0]
      vCommandeLigne.numeroLigne = vData[1]
      vCommandeLigne.codeArticle = vData[21]
      vCommandeLigne.typeLigne = vData[3]
      vCommandeLigne.designation = vData[4]
      vCommandeLigne.quantite = vData[5]
      vCommandeLigne.prixUnitaire = vData[6]
      vCommandeLigne.prixTotalLigne = vData[7]
      vCommandeLigne.totalLigneHT = vData[8]
      vCommandeLigne.totalTTCLigne = vData[9]
      vCommandeLigne.codeTVA = vData[10]
      vCommandeLigne.tauxTVA = vData[11]
      vCommandeLigne.codeTiers = vData[12]      
    end
    
    return vListCommandeLigne.empty? ? nil : vListCommandeLigne
    
  end
  
  def insertCommandeLigne(pCommandeLigne)
    
    req = "INSERT INTO LigneCdeClient (#{@entete})"
    req << " VALUES ('#{pCommandeLigne.codeDocument}', '#{pCommandeLigne.numeroLigne}', '#{pCommandeLigne.codeArticle}', '#{pCommandeLigne.typeLigne}', '#{pCommandeLigne.nomArticle}',"\
           +"'#{pCommandeLigne.quantite}', '#{pCommandeLigne.prixUnitaire}', '#{pCommandeLigne.prixTotalLigne}', '#{pCommandeLigne.totalLigneHT}', '#{pCommandeLigne.totalTTCLigne}',"\
           +"'#{pCommandeLigne.codeTVA}', '#{pCommandeLigne.tauxTVA}', '#{pCommandeLigne.codeTiers}')"
    vMessage = ""
    vReussite = false
    begin
      @connexion.execute(req)
      vMessage = "====== La Ligne de commande #{pCommandeLigne.numeroLigne} a été intégré à API (#{pCommandeLigne.nomArticle})"
      vReussite = true
    rescue
      vMessage = "====== !!!!! La Ligne de commande #{pCommandeLigne.numeroLigne} n'a pas été intégré à API (#{pCommandeLigne.nomArticle})"
      vCommandeDao = CommandeDaoDb.new(@connexion)
      vCommandeDao.deleteCommande(pCommandeLigne.codeDocument)
    end
    
    puts vMessage
    return [vReussite, vMessage]
    
  end
  
  def deleteCommandeLigne(pCommandeId)
    req = "DELETE FROM LigneCdeClient WHERE CodeDocument='#{pCommandeId}'"
    @connexion.execute(req)
    puts "Suppression des lignes de commande dont le code est #{pCommandeId}"
  end
  
end
# Implémentation de la classe CommandeDaoCsv
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

class CommandeDaoCsv  
  
  def initialize(pConnexion)
    @connexion = pConnexion
  end
  
  def getListCommande
      
    vListCommande = []
    i=1
    
    @connexion.data.each do |ligne|
      vCommandeLigne = CommandeLigne.new()
      vCommandeLigne.codeDocument = ligne[1]
      vCommandeLigne.numeroLigne = i
      vCommandeLigne.codeArticle = ligne[96]
      vCommandeLigne.nomArticle = ligne[98]
      vCommandeLigne.quantite = ligne[99]
      vCommandeLigne.prixUnitaire = ligne[100]
      vCommandeLigne.prixTotalLigne = ligne[105]
      vCommandeLigne.totalTTCLigne = ligne[105]
      vCommandeLigne.codeTVA = 1
      vCommandeLigne.tauxTVA = ligne[102]
      vCommandeLigne.codeTiers = ligne[3]
      vCommandeLigne.typeLigne = 'U'
      vCommandeLigne.totalLigneHT = (ligne[105].to_f / (1 + ligne[102].to_f / 100)).round(2).to_s.gsub(".",",")
      i += 1
              
      num = nil
      vListCommande.each_index do |j|
        if vListCommande[j].code==ligne[1]
          num = j
          i = 1
          break
        end
      end
      
      if num  
        vListCommande[num].ligneCommande << vCommandeLigne
      else
        vCommande = Commande.new(ligne[1])

        vCommande.date = DateTime.parse(ligne[0])
        vCommande.type = 'CC'
        vCommande.client.email = ligne[2]
        vCommande.client.code = ligne[3]
        vCommande.client.facNom = "#{ligne[6]} #{ligne[5]}"
        vCommande.client.facCivilite = ligne[4]
        vCommande.client.facAdr = ligne[8]
        vCommande.client.facCp = ligne[17]
        vCommande.client.facVille = ligne[18]
        vCommande.client.facTel = ligne[18]
        vCommande.client.facTel2 = ligne[18]
        vCommande.client.dateCreation = DateTime.now
        vCommande.client.livNom = "#{ligne[29]} #{ligne[28]}"
        vCommande.client.livCivilite = ligne[27]
        vCommande.client.livAdr = ligne[30]
        vCommande.client.livCp = ligne[39]
        vCommande.client.livVille = ligne[40]
        vCommande.client.livTel = ligne[44]
        vCommande.totalTTC = ligne[49]
        vCommande.totalTvaNet1 = ligne[52]
        vCommande.tarif = 1
        vCommande.typeDeTarif = 'T'
        vCommande.devise = 'E'
        vCommande.categorieAV = '0'
        vCommande.ligneCommande << vCommandeLigne
        
        vListCommande << vCommande
                
      end
    end
    
    return vListCommande
    
  end
  
end
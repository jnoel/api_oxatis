# Implémentation de la classe Client
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

class Client
  attr_accessor :email, :code, :facNom, :facCivilite, :facAdr, 
                :facCp, :facVille, :facTel, :facTel2, :dateCreation,
                :livNom, :livCivilite, :livAdr, :livCp, :livVille,
                :livTel, :livTel2
    
  def initialize()
   
  end
  
  def to_s
    return "client{email => #{email}, code => #{code}, facNom => #{facNom}"
  end

end
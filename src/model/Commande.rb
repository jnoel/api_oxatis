# Implémentation de la classe Commande
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# coding: utf-8

class Commande
  attr_accessor :code, :type, :date, :client, :tarif, :typeDeTarif, :devise, :categorieAV, 
                :totalTTC, :totalTvaNet1, :ligneCommande
                   
  def initialize(pCode)
    @code = pCode
    @ligneCommande = []
    @client = Client.new
  end
  
  def to_s
    return "commande{code => #{@code}, type => #{@type}, date => #{@date.strftime("%d/%m/%Y")}}"
  end

end
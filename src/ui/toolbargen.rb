# coding: utf-8

# UI: Barre de menu
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class ToolBarGenUi < Gtk::Toolbar

  def initialize window
  
    super() 
    set_toolbar_style Gtk::Toolbar::Style::BOTH
    
    rechargetb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./src/ui/resources/icon.png" ), :label => "Commandes" )
    configtb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./src/ui/resources/configuration.png" ), :label => "Configuration" )
    abouttb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./src/ui/resources/about.png" ), :label => "A Propos" )
    quittb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./src/ui/resources/exit.png" ), :label => "Quitter" )
        
    
    configtb.signal_connect( "clicked" ) {
      config = ConfigurationUi.new window
      config.run { |response| 
        if response==Gtk::ResponseType::OK
          window.message_info "Vous avez modifier la configuration.\nVous devez redémarrer le programme\npour qu'elle soit prise en compte."
        end
      }
      config.destroy
    } 
    
    abouttb.signal_connect( "clicked" ) { 
      about = AboutUi.new 
      about.signal_connect('response') { about.destroy }
    }
      
    quittb.signal_connect( "clicked" ) { 
      window.quit
    }
      
    tool = [rechargetb, Gtk::SeparatorToolItem.new, configtb, Gtk::SeparatorToolItem.new, abouttb, quittb]
    
    tool.each_index { |i| 
      self.insert tool[i],i
    }
  
  end

end
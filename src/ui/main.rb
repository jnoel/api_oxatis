# coding: utf-8

# UI: Fenêtre principale
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class MainWindowUi < Gtk::Window
	
	attr_reader :contact, :liste_contact, :version
	
	def initialize level
	
		super
		
		signal_connect( "destroy" ) { Gtk.main_quit }

		set_title GLib.application_name
		
		set_window_position :center
		set_default_size 800, 600
		set_icon( "./src/ui/resources/icon.png" )	
		
		lancement
	
	end
	
	def lancement
		
		@toolbargen = ToolBarGenUi.new self
		@liste_contact = ListeCommandeImportUi.new self
		
		@vbox = Gtk::Box.new :vertical, 2
		@vbox.pack_start @toolbargen, :expand => false, :fill => false, :padding => 0
		@vbox.pack_start @liste_contact, :expand => true
		
		@liste_contact.refresh
		
		add @vbox
		
		show_all
	
	end
	
	def efface_vbox_actuelle
	
		@vbox.remove @vbox.children[@vbox.children.count-1] if @vbox.children.count.eql?(2)
	
	end
	
	def affiche vbox
	
		efface_vbox_actuelle
		@vbox.pack_start vbox, true
        self.show_all
	
	end	
	
	def quit
		
		dialog = Gtk::MessageDialog.new(:parent => self, 
                                    :flags => :destroy_with_parent,
                                    :type => :question,
                                    :buttons_type => :yes_no,
                                    :message => "Voulez-vous réellement quitter #{GLib.application_name} ?")
		response = dialog.run
		case response
  		when Gtk::ResponseType::YES
  			Gtk.main_quit
		end 
		dialog.destroy
		
	end
	
	def message_erreur message, console=true
		puts message if console
		dialog = Gtk::MessageDialog.new(:parent => self, 
                                    :flags => :destroy_with_parent,
                                    :type => :error,
                                    :buttons_type => :ok,
                                    :message => message)
		response = dialog.run
		dialog.destroy
	end
	
	def message_info message, console=true
		puts message if console
		dialog = Gtk::MessageDialog.new(:parent => self, 
                                    :flags => :destroy_with_parent,
                                    :type => :info,
                                    :buttons_type => :ok,
                                    :message => message)
		response = dialog.run
		dialog.destroy
	end

end

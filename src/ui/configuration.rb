# coding: utf-8

# UI: Fenêtre de configuration
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class ConfigurationUi < Gtk::Dialog
  
  def initialize window
    
    super(:title => "Configuration", 
          :parent => window, 
          :flags => :destroy_with_parent,
          :buttons => nil)
    
    set_default_size 500, 150
    set_window_position :center
    
    @chemin_conf = './configuration.yaml'
    @conf = YAML.load_file(@chemin_conf)
    @vApiFile=@conf["ApiFile"]
    @vOxatisFile=@conf["OxatisFile"]
      
    self.child.pack_start chemins, :expand => true, :fill => true
    self.child.add boutons, :expand => false, :fill => false
    self.child.show_all

  end
  
  def chemins
    table = Gtk::Table.new(3, 3, false)
    
    i=0
    align0 = Gtk::Alignment.new 0, 0.5, 0, 0
    align0.add Gtk::Label.new( "Base de données Api :" )
    table.attach( align0, 0, 1, i, i+1, :fill, :fill, 5, 5 )
    @apiFile = Gtk::Entry.new
    @apiFile.text = @vApiFile
    @apiFile.editable = false
    table.attach( @apiFile, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
    buttonApi = Gtk::Button.new :label => "..."
    table.attach( buttonApi, 2, 3, i, i+1, :shrink, :shrink, 5, 5 )
    
    i+=1
    align3 = Gtk::Alignment.new 0, 0.5, 0, 0
    align3.add Gtk::Label.new( "Fichier CSV Oxatis :" )
    table.attach( align3, 0, 1, i, i+1, :fill, :fill, 5, 5 )
    @oxatisFile = Gtk::Entry.new
    @oxatisFile.text = @vOxatisFile
    @oxatisFile.editable = false
    table.attach( @oxatisFile, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
    buttonOxatis = Gtk::Button.new :label => "..."
    table.attach( buttonOxatis, 2, 3, i, i+1, :shrink, :shrink, 5, 5 )
    
    buttonApi.signal_connect('clicked') {
      fichier = chercheFichier "Gestc.mdb"
      @apiFile.text = fichier if fichier
    }
    
    buttonOxatis.signal_connect('clicked') {
      fichier = chercheFichier "*.csv"
      @oxatisFile.text = fichier if fichier
    }
    
    table
  end
  
  def chercheFichier filtre=nil
    dialog = Gtk::FileChooserDialog.new(:title => "Open File",
                                        :parent => @window,
                                        :action => :open)
    dialog.add_button("Choisir", Gtk::ResponseType::ACCEPT)
    dialog.add_button("Annuler", Gtk::ResponseType::CANCEL)
    if filtre
      filter = Gtk::FileFilter.new
      filter.add_pattern(filtre)
      dialog.filter = filter
    end
    dialog.set_window_position :center
    fichier = nil
    if dialog.run == Gtk::ResponseType::ACCEPT
      fichier = dialog.filename
    end
    dialog.destroy
    
    return fichier
  end
  
  def boutons
    align0 = Gtk::Alignment.new 1, 1, 0, 0
    hbox = Gtk::Box.new :horizontal, 3
    
    annuler = Gtk::Button.new
    hbox_annuler = Gtk::Box.new :horizontal, 2
    hbox_annuler.add Gtk::Image.new( :file => "./src/ui/resources/cancel.png" )
    hbox_annuler.add Gtk::Label.new "Annuler"
    annuler.add hbox_annuler
    
    enregister = Gtk::Button.new 
    hbox_enregister = Gtk::Box.new :horizontal, 2
    hbox_enregister.add Gtk::Image.new( :file => "./src/ui/resources/ok.png" )
    hbox_enregister.add Gtk::Label.new "Enregister"
    enregister.add hbox_enregister
    
    hbox.add enregister
    hbox.add annuler
    align0.add hbox
    
    annuler.signal_connect('clicked') {
      response Gtk::ResponseType::CANCEL 
    }
    
    enregister.signal_connect('clicked') {
      @conf["ApiFile"] = @apiFile.text
      @conf["OxatisFile"] = @oxatisFile.text
      
      File.open(@chemin_conf, 'w') do |out|
          YAML.dump(@conf, out)
      end
      response Gtk::ResponseType::OK 
    }
    
    align0
  end
  
end
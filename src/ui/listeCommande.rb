# coding: utf-8

# UI: Fenêtre de la liste des commandes Oxatis
# @author 2014 Jean-Noël Rouchon <mail@mithril.re>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#  
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#  
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class ListeCommandeImportUi < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@batch = CommandeRecupBatch.new
		 
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::Box.new :vertical, 3
					
		frame = Gtk::Frame.new "Liste des commandes à importer"
		@compteur = Gtk::Label.new
		importBt = Gtk::Button.new
		hbox_import = Gtk::Box.new :horizontal, 2
		hbox_import.add Gtk::Image.new( :file => "./src/ui/resources/add.png" )
		hbox_import.add Gtk::Label.new "Importer"
		importBt.add hbox_import
		rechargeBt = Gtk::Button.new
		hbox_recharge = Gtk::Box.new :horizontal, 2
    hbox_recharge.add Gtk::Image.new( :file => "./src/ui/resources/refresh.png" )
    hbox_recharge.add Gtk::Label.new "Recharger"
    rechargeBt.add hbox_recharge
		
		hbox = Gtk::Box.new :horizontal, 3
		hbox.pack_start rechargeBt, :expand => false, :fill => false, :padding => 3
		hbox.pack_start importBt, :expand => false, :fill => false, :padding => 3
		
		# Agencement
		vbox.pack_start hbox, :expand => false, :fill => false, :padding => 3
		vbox.pack_start tableauCommandes, :expand => true, :fill => true, :padding => 3 
		vbox.pack_start @compteur, :expand => false, :fill => false, :padding => 3 

		frame.add vbox
		
		importBt.signal_connect('clicked') {
			traiteRetour @batch.insertCommande
		}
		
		rechargeBt.signal_connect('clicked') {
      refresh
    }
		
		pack_start frame, :expand => true, :fill => true, :padding => 3 
	
	end
	
	def tableauCommandes
	
		# list_store 					           (num    client   art     qte     pu     total)
		@list_store = Gtk::TreeStore.new(String, String, String, String, String, String)
		@view = Gtk::TreeView.new(@list_store)
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Num Commande", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Client", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Article", renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Qtité", renderer_left, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("PU", renderer_left, :text => 4)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Total", renderer_left, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic,:automatic)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def refresh
    remplir @batch.recupCommande
	end
	
	def remplir res

		@list_store.clear
		
		res.each { |c|			
			iter = @list_store.append nil
			iter[0] = c.code
			iter[1] = c.client.facNom
			iter[5] = c.totalTTC
		}
			
	end
	
	def traiteRetour pListMessage
	  vMessages = ""
	  pInfo = true
	  pListMessage.each do |vMes|
	    pInfo &= vMes[0]
	    vMes[1].each do |m|
	      vMessages << "#{m[:message]}\n"
	    end
	  end
	  
	  if pInfo
      @window.message_info vMessages, console=false
    else
      @window.message_erreur vMessages, console=false
    end
	end
	
end

